package com.youcode.gathergrid.service;

import com.youcode.gathergrid.entities.Event;

public interface EventService {
    public void save(Event event);
}
