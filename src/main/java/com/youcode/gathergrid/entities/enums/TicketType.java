package com.youcode.gathergrid.entities.enums;

public enum TicketType {
    STANDARD,
    VIP,
    PREMIUM,
}
