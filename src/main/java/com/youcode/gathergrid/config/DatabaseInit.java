package com.youcode.gathergrid.config;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.http.HttpServlet;

public class DatabaseInit extends HttpServlet {
    @Override
    public void init() {
        this.initializeDatabase();
    }

    public void initializeDatabase() {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("persistencejakartaee.grather_grid");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        entityManager.getTransaction().commit();
        entityManager.close();
        entityManagerFactory.close();
    }
}
