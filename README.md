# GatherGrid Project README

## Table of Contents

- [Introduction](#introduction)
- [Project Structure](#project-structure)
- [Dependencies](#dependencies)
- [Configuration](#configuration)
- [Running the Project](#running-the-project)

## Introduction

GatherGrid is a Java EE web application that demonstrates how to set up a basic web project using Jakarta EE, Hibernate, and MySQL. This project serves as a starting point for building web applications with these technologies.

## Project Structure

The project has the following structure:


````
GatherGrid/                  - Project documentation (you are here)
├───livrable
├───src
│   ├───main
│   │   ├───docker
│   │   ├───java
│   │   │   └───com
│   │   │       └───youcode
│   │   │           └───gathergrid
│   │   │               ├───config
│   │   │               ├───entities
│   │   │               ├───repository
│   │   │               │   └───implementation
│   │   │               ├───service
│   │   │               │   └───implementation
│   │   │               └───servlet
│   │   ├───resources
│   │   │   └───META-INF
│   │   └───webapp
│   │       └───WEB-INF                             - Web application resources
│   └───test
│       ├───java
│       └───resources
├── pom.xml                                         - Maven project configuration
└── README.md                                       - Project documentation (you are here)


````
## Dependencies

The project uses the following dependencies managed by Maven:

- Jakarta EE 8: Java EE platform for building enterprise-level applications.
- Hibernate 5.6.15.Final: JPA provider for object-relational mapping.
- MySQL Connector/J 8.0.33: MySQL JDBC driver for database connectivity.
- Lombok 1.18.28: A library to reduce boilerplate code in Java.

## Configuration

### `persistence.xml`

The `persistence.xml` file located in `src/main/resources/META-INF` defines the persistence unit and database configuration. It sets up the connection to a MySQL database and specifies Hibernate as the JPA provider.

### `web.xml`

The `web.xml` file in the `webapp/WEB-INF` directory configures servlets and listeners for the application. It includes servlet mappings and initializes the database.

### MavenThe 
`pom.xml` file at the project root defines project configuration and dependencies. It sets the project name, version, and specifies the build plugins.

## Running the Project

To run the GatherGrid project:

1. Ensure you have Maven installed.
2. Build the project by running `mvn clean install` in the project root directory.
3. Deploy the generated WAR file to a Jakarta EE compatible server.
4. Access the application by navigating to the appropriate URL (e.g., `http://localhost:8080/GatherGrid_war/`).
